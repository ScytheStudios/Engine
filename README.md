# Igni Engine (JavaFX Edition) Source Code  
  
## Features  
**Rendering Engine**  
Wiremesh  
Filled Objects (Not Implemented)  
Textured Object (Not Implemented)  
  
**Physics Engine**  
Player Movement  
Player Collision (Not Implemented)  
Gravity (Not Implemented)  