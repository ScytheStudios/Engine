package Source;

import java.awt.MouseInfo;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

public class Entity {

    String Type;
    double Width;
    double Height;
    double Depth;
    boolean Visible;
    boolean Transparent;
    Vertex[] V;
    Vertex Location;
    Vertex Velocity;
    Vertex Angle;
    Vertex AngleVelocity;
    Tesseract TesseractInstance;
    Cube CubeInstance;
    Square SquareInstance;
    Triangle TriangleInstance;
    XLine LineInstance;
    Vertex VertexInstance;
    Color Col;
    Color Col2;

    public Entity() {
        this.Type = "null";
        this.Location = new Vertex(0, 0, 0);
        this.Velocity = new Vertex(0, 0, 0);
        this.Angle = new Vertex(0, 0, 0);
        this.AngleVelocity = new Vertex(0, 0, 0);
        this.Width = 0;
        this.Height = 0;
        this.Depth = 0;
        this.Col = Color.rgb(0, 0, 0);
        this.Col2 = Color.rgb(0, 0, 0);
    }

    public void SetType(String Type) {
        this.Type = Type;
    }

    public String GetType() {
        return this.Type;
    }

    public void SetLocation(double X, double Y, double Z) {
        this.Location.X = X;
        this.Location.Y = Y;
        this.Location.Z = Z;
        //We Don't Use this.Location = New Vertex(X, Y, Z) Because We Don't Want To Create A New Vertex, Just Want To Modify It
    }

    public Vertex GetLocation() {
        return this.Location;
    }

    public void SetVelocity(double X, double Y, double Z) {
        this.Velocity.X = X;
        this.Velocity.Y = Y;
        this.Velocity.Z = Z;
        //We Don't Use this.Velocity = New Vertex(X, Y, Z) Because We Don't Want To Create A New Vertex, Just Want To Modify It
    }

    public Vertex GetVelocity() {
        return this.Velocity;
    }

    public void SetAngle(double X, double Y, double Z) {
        this.Angle.X = X;
        this.Angle.Y = Y;
        this.Angle.Z = Z;
        //We Don't Use this.Angle = New Vertex(X, Y, Z) Because We Don't Want To Create A New Vertex, Just Want To Modify It
    }

    public Vertex GetAngle() {
        return this.Angle;
    }

    public void SetAngleVelocity(double X, double Y, double Z) {
        this.AngleVelocity.X = X;
        this.AngleVelocity.Y = Y;
        this.AngleVelocity.Z = Z;
        //We Don't Use this.AngleVelocity = New Vertex(X, Y, Z) Because We Don't Want To Create A New Vertex, Just Want To Modify It
    }

    public Vertex GetAngleVelocity() {
        return this.AngleVelocity;
    }

    public void SetColor(int R, int G, int B) {
        this.Col = Color.rgb(R, G, B);
    }

    public Color GetColor() {
        return this.Col;
    }

    public void SetWidth(double Width) {
        if (!this.Type.equals("Tesseract") && !this.Type.equals("Cube") && !this.Type.equals("Square")) {
            System.out.println("Syntax Error : Only Cubes / Squares Can Have Their Width Set");
            return;
        }
        this.Width = Width;
    }

    public double GetWidth() {
        return this.Width;
    }

    public void SetHeight(double Height) {
        if (!this.Type.equals("Tesseract") && !this.Type.equals("Cube") && !this.Type.equals("Square")) {
            System.out.println("Syntax Error : Only Cubes / Squares Can Have Their Height Set");
            return;
        }
        this.Height = Height;
    }

    public double GetHeight() {
        return this.Height;
    }

    public void SetDepth(double Depth) {
        if (!this.Type.equals("Tesseract") && !this.Type.equals("Cube")) {
            System.out.println("Syntax Error : Only Cubes Can Have Their Depth Set");
            return;
        }
        this.Depth = Depth;
    }

    public double GetDepth() {
        return this.Depth;
    }

    public void Create() {
        if (this.Type.equals("Cube")) {
            CubeInstance = new Cube(this.Width, this.Height, this.Depth);
            this.V = CubeInstance.GetVertex();
            this.Visible = true;
            return;
        }
        if (this.Type.equals("Tesseract")) {
            TesseractInstance = new Tesseract(this.Width, this.Height, this.Depth);
            this.V = TesseractInstance.GetVertex();
            this.Visible = true;
            return;
        }
        System.out.println("Syntax Error : Invalid Entity Type");
    }

    public void Update() {
        if (this.Type.equals("Player")) {
            this.SetVelocity(0, 0, 0);
            double Acceleration = 5;
            if (Main.HoldW == true) {
                this.Velocity.X = this.Velocity.X - Acceleration * Math.sin(this.Angle.X);
                this.Velocity.Z = this.Velocity.Z + Acceleration * Math.cos(this.Angle.X);
            }
            if (Main.HoldS == true) {
                this.Velocity.X = this.Velocity.X + Acceleration * Math.sin(this.Angle.X);
                this.Velocity.Z = this.Velocity.Z - Acceleration * Math.cos(this.Angle.X);
            }
            if (Main.HoldA == true) {
                this.Velocity.X = this.Velocity.X - Acceleration * Math.cos(this.Angle.X);
                this.Velocity.Z = this.Velocity.Z - Acceleration * Math.sin(this.Angle.X);
            }
            if (Main.HoldD == true) {
                this.Velocity.X = this.Velocity.X + Acceleration * Math.cos(this.Angle.X);
                this.Velocity.Z = this.Velocity.Z + Acceleration * Math.sin(this.Angle.X);
            }
            if (Main.HoldSPACE == true) {
                this.Velocity.Y = this.Velocity.Y + 5;
            }
            if (Main.HoldC == true) {
                this.Velocity.Y = this.Velocity.Y - 5;
            }
            double MouseX = MouseInfo.getPointerInfo().getLocation().getX();
            double MouseY = MouseInfo.getPointerInfo().getLocation().getY();
            double DeltaX = (Main.ScreenWidth / 2) - MouseX;
            double DeltaY = (Main.ScreenHeight / 2) - MouseY;
            this.Angle.X = this.Angle.X + DeltaX / 1250;
            //this.Angle.Y = this.Angle.Y + DeltaY / 1250;
        }
        this.Location.X = this.Location.X + this.Velocity.X;
        this.Location.Y = this.Location.Y + this.Velocity.Y;
        this.Location.Z = this.Location.Z + this.Velocity.Z;
        this.Angle.X = this.Angle.X + this.AngleVelocity.X;
        this.Angle.Y = this.Angle.Y + this.AngleVelocity.Y;
        this.Angle.Z = this.Angle.Z + this.AngleVelocity.Z;
    }

    public Group Render() {
        Group Rendered = new Group();
        if (this.Visible == true) {
            if (this.Type.equals("Cube")) {
                Rendered = CubeInstance.Render(this.V, this.Location, this.Angle, this.Col, this.Col2);
            }
            if (this.Type.equals("Tesseract")) {
                Rendered = TesseractInstance.Render(this.V, this.Location, this.Angle, this.Col);
            }
        }
        return Rendered;
    }

}

class Renderer extends Main {

    public double GetScreenX(double X, double Z) {
        return (Main.ScreenWidth / 2) + (750 * (X / Z));
    }

    public double GetScreenY(double Y, double Z) {
        return (Main.ScreenHeight / 2) + (750 * (-Y / Z));
    }

    public double GetRenderLength(double VX, double VY, double VZ) {
        double DeltaX = VX - Player.Location.X;
        double DeltaY = VY - Player.Location.Y;
        double DeltaZ = VZ - Player.Location.Z;
        double Length = Math.sqrt((DeltaX * DeltaX) + (DeltaY * DeltaY) + (DeltaZ * DeltaZ));
        return Length;
    }

    public int GetBrushSize(double VX, double VY, double VZ) {
        int Size = (int) (5000 / GetRenderLength(VX, VY, VZ));
        return Size;
    }

    public Group RenderLine(Vertex V, Vertex Target, Vertex Location, Vertex Angle, Color Col, int BrushSize) {
        Line Rendered = new Line();
        Vertex VisualLocation = GetVisualLocation(V.X, V.Y, V.Z, Location.X, Location.Y, Location.Z, Angle.X, Angle.Y, Angle.Z);
        Vertex TargetVisualLocation = GetVisualLocation(Target.X, Target.Y, Target.Z, Location.X, Location.Y, Location.Z, Angle.X, Angle.Y, Angle.Z);
        double ScreenX = GetScreenX((VisualLocation.X), (VisualLocation.Z));
        double ScreenY = GetScreenY((VisualLocation.Y), (VisualLocation.Z));
        double TargetScreenX = GetScreenX((TargetVisualLocation.X), (TargetVisualLocation.Z));
        double TargetScreenY = GetScreenY((TargetVisualLocation.Y), (TargetVisualLocation.Z));
        Rendered.setStartX(ScreenX);
        Rendered.setStartY(ScreenY);
        Rendered.setEndX(TargetScreenX);
        Rendered.setEndY(TargetScreenY);
        Rendered.setStroke(Col);
        Rendered.setStrokeWidth(BrushSize);
        return new Group(Rendered);
    }

    public Group RenderPolygon(Vertex V, Vertex V2, Vertex V3, Vertex V4, Vertex Location, Vertex Angle, Color Col, int BrushSize, String TextureURL) {
        Polygon Rendered = new Polygon();
        Vertex VisualLocation = GetVisualLocation(V.X, V.Y, V.Z, Location.X, Location.Y, Location.Z, Angle.X, Angle.Y, Angle.Z);
        Vertex VisualLocation2 = GetVisualLocation(V2.X, V2.Y, V2.Z, Location.X, Location.Y, Location.Z, Angle.X, Angle.Y, Angle.Z);
        Vertex VisualLocation3 = GetVisualLocation(V3.X, V3.Y, V3.Z, Location.X, Location.Y, Location.Z, Angle.X, Angle.Y, Angle.Z);
        Vertex VisualLocation4 = GetVisualLocation(V4.X, V4.Y, V4.Z, Location.X, Location.Y, Location.Z, Angle.X, Angle.Y, Angle.Z);
        double ScreenX = GetScreenX((VisualLocation.X), (VisualLocation.Z));
        double ScreenY = GetScreenY((VisualLocation.Y), (VisualLocation.Z));
        double ScreenX2 = GetScreenX((VisualLocation2.X), (VisualLocation2.Z));
        double ScreenY2 = GetScreenY((VisualLocation2.Y), (VisualLocation2.Z));
        double ScreenX3 = GetScreenX((VisualLocation3.X), (VisualLocation3.Z));
        double ScreenY3 = GetScreenY((VisualLocation3.Y), (VisualLocation3.Z));
        double ScreenX4 = GetScreenX((VisualLocation4.X), (VisualLocation4.Z));
        double ScreenY4 = GetScreenY((VisualLocation4.Y), (VisualLocation4.Z));
        Rendered.getPoints().addAll(ScreenX, ScreenY, ScreenX2, ScreenY2, ScreenX3, ScreenY3, ScreenX4, ScreenY4);
        Image Texture;
        TextureURL = TextureURL + ".png";
        try {
            Texture = new Image(TextureURL);
            Rendered.setFill(new ImagePattern(Texture, 0, 0, 1, 1, true));
        }
        catch(Exception e) {
            try {
                Texture = new Image("Missing.png");
                Rendered.setFill(new ImagePattern(Texture, 0, 0, 1, 1, true));
            }
            catch(Exception e2) {
                Rendered.setFill(Col);
            }
        }
        Rendered.setStrokeWidth(0);
        return new Group(Rendered);
    }

    public Vertex GetVisualLocation(double VX, double VY, double VZ, double LocationX, double LocationY, double LocationZ, double AngleX, double AngleY, double AngleZ) {
        Vertex VisualLocation = new Vertex(0, 0, 0);
        double InternalLengthXAxis = Math.sqrt((VX * VX) + (VZ * VZ));
        double InternalLengthYAxis = Math.sqrt((VY * VY) + (VZ * VZ));
        double InternalAngleX = Math.atan2(VX, VZ);
        double InternalAngleY = Math.atan2(VY, VZ);
        VX = (InternalLengthXAxis * Math.sin(AngleX + InternalAngleX));
        VZ = (InternalLengthXAxis * Math.cos(AngleX + InternalAngleX));
        double LengthX = LocationX + VX - Player.Location.X;
        double LengthY = LocationY + VY - Player.Location.Y;
        double LengthZ = LocationZ + VZ - Player.Location.Z;
        double OldXAngle = Math.atan2(LengthX, LengthZ);
        double OldYAngle = Math.atan2(LengthY, LengthZ);
        double LengthXAxis = Math.sqrt((LengthX * LengthX) + (LengthZ * LengthZ));
        double LengthYAxis = Math.sqrt((LengthY * LengthY) + (LengthZ * LengthZ));
        double Length = Math.sqrt((LengthX * LengthX) + (LengthY * LengthY) + (LengthZ * LengthZ));
        VisualLocation.X = (LengthXAxis * Math.sin(Player.Angle.X + OldXAngle));
        VisualLocation.Y = LengthY;
        VisualLocation.Z = (LengthXAxis * Math.cos(Player.Angle.X + OldXAngle));
        return VisualLocation;
    }

    public boolean InsidePlayerFOV(double VX, double VY, double VZ, double LocationX, double LocationY, double LocationZ) {
        double LengthX = LocationX + VX - Player.Location.X;
        double LengthY = LocationY + VY - Player.Location.Y;
        double LengthZ = LocationZ + VZ - Player.Location.Z;
        double LengthXAxis = Math.sqrt((LengthX * LengthX) + (LengthZ * LengthZ));
        double AngleX = Math.toDegrees(Math.atan2(LengthX, LengthZ) + Player.Angle.X);
        double AngleY = Math.toDegrees(Math.atan2(LengthY, LengthXAxis) + Player.Angle.Y);
        while (AngleX > 180) {
            AngleX = AngleX - 360;
        }
        while (AngleX < -180) {
            AngleX = AngleX + 360;
        }
        while (AngleY > 180) {
            AngleY = AngleY - 360;
        }
        while (AngleY < -180) {
            AngleY = AngleY + 360;
        }
        if (AngleX < 65 && AngleX > - 65) {
            if (AngleY < 65 && AngleY > - 65) {
                return true;
            }
        }
        return false;
    }

}

class Vertex {

    double X;
    double Y;
    double Z;

    public Vertex(double X, double Y, double Z) {
        this.X = X;
        this.Y = Y;
        this.Z = Z;
    }

    public double GetX() {
        return this.X;
    }

    public double GetY() {
        return this.Y;
    }

    public double GetZ() {
        return this.Z;
    }

}

class XLine {

    Vertex[] V = new Vertex[2];

    public XLine(Vertex V, Vertex V2) {
        this.V[1] = V;
        this.V[2] = V2;
    }

    public Vertex[] GetVertex() {
        return this.V;
    }

}

class Triangle {

    Vertex[] V = new Vertex[3];

    public Triangle(Vertex V, Vertex V2, Vertex V3) {
        this.V[1] = V;
        this.V[2] = V2;
        this.V[3] = V3;
    }

    public Vertex[] GetVertex() {
        return this.V;
    }

}

class Square {

    Vertex[] V = new Vertex[4];

    public Square(double Width, double Height) {

    }

    public Vertex[] GetVertex() {
        return this.V;
    }

}

class Cube {

    public Vertex[] V = new Vertex[8];

    public Cube(double Width, double Height, double Depth) {
        double W = Width / 2;
        double H = Height / 2;
        double D = Depth / 2;
        this.V[0] = new Vertex(-W, H, -D);
        this.V[1] = new Vertex(-W, H, D);
        this.V[2] = new Vertex(W, H, D);
        this.V[3] = new Vertex(W, H, -D);
        this.V[4] = new Vertex(-W, -H, -D);
        this.V[5] = new Vertex(-W, -H, D);
        this.V[6] = new Vertex(W, -H, D);
        this.V[7] = new Vertex(W, -H, -D);
    }

    public Vertex[] GetVertex() {
        return this.V;
    }

    public Group Render(Vertex[] V, Vertex Location, Vertex Angle, Color Col, Color Col2) {
        Group Rendered = new Group();
        int BrushSize = new Renderer().GetBrushSize(Location.X, Location.Y, Location.Z);
        if (new Renderer().InsidePlayerFOV(0, 0, 0, Location.X, Location.Y, Location.Z)) {
            Rendered.getChildren().add(new Renderer().RenderPolygon(V[0], V[1], V[2], V[3], Location, Angle, Col, BrushSize, "Stone"));
            Rendered.getChildren().add(new Renderer().RenderPolygon(V[4], V[5], V[6], V[7], Location, Angle, Col, BrushSize, "Stone"));
            Rendered.getChildren().add(new Renderer().RenderPolygon(V[0], V[1], V[5], V[4], Location, Angle, Col, BrushSize, "Stone"));
            Rendered.getChildren().add(new Renderer().RenderPolygon(V[2], V[3], V[7], V[6], Location, Angle, Col, BrushSize, "Stone"));
            Rendered.getChildren().add(new Renderer().RenderPolygon(V[1], V[2], V[6], V[5], Location, Angle, Col, BrushSize, "Stone"));
            Rendered.getChildren().add(new Renderer().RenderPolygon(V[0], V[3], V[7], V[4], Location, Angle, Col, BrushSize, "Stone"));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[0], V[1], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[1], V[2], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[2], V[3], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[3], V[0], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[4], V[5], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[5], V[6], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[6], V[7], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[7], V[4], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[0], V[4], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[1], V[5], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[2], V[6], Location, Angle, Col2, BrushSize));
            //Rendered.getChildren().add(new Renderer().RenderLine(V[3], V[7], Location, Angle, Col2, BrushSize));
        }
        return Rendered;
    }

}

class Tesseract {

    public Vertex[] V = new Vertex[16];
    public Vertex[] Sign = new Vertex[16];
    public double Width;
    public double Height;
    public double Depth;
    public double InnerAngle;

    public Tesseract(double Width, double Height, double Depth) {
        this.Width = Width;
        this.Height = Height;
        this.Depth = Depth;
        double OW = Width * 1.5;
        double OH = Height * 1.5;
        double OD = Depth * 1.5;
        double IW = Width * 0.5;
        double IH = Depth * 0.5;
        double ID = Height * 0.5;
        this.Sign[0] = new Vertex(-1, 1, -1);
        this.Sign[1] = new Vertex(-1, 1, 1);
        this.Sign[2] = new Vertex(1, 1, 1);
        this.Sign[3] = new Vertex(1, 1, -1);
        this.Sign[4] = new Vertex(-1, -1, -1);
        this.Sign[5] = new Vertex(-1, -1, 1);
        this.Sign[6] = new Vertex(1, -1, 1);
        this.Sign[7] = new Vertex(1, -1, -1);
        this.Sign[8] = new Vertex(-1, 1, -1);
        this.Sign[9] = new Vertex(-1, 1, 1);
        this.Sign[10] = new Vertex(1, 1, 1);
        this.Sign[11] = new Vertex(1, 1, -1);
        this.Sign[12] = new Vertex(-1, -1, -1);
        this.Sign[13] = new Vertex(-1, -1, 1);
        this.Sign[14] = new Vertex(1, -1, 1);
        this.Sign[15] = new Vertex(1, -1, -1);
        for (int x = 0; x < 8; x++) {
            this.V[x] = new Vertex(Sign[x].X * OW, Sign[x].Y * OH, Sign[x].Z * OD);
        }
        for (int x = 8; x < 16; x++) {
            this.V[x] = new Vertex(Sign[x].X * IW, Sign[x].Y * IH, Sign[x].Z * ID);
        }
    }

    public Vertex[] GetVertex() {
        return this.V;
    }

    public void Animate() {
        this.InnerAngle = this.InnerAngle + 0.05;
        //this.InnerAngle = 180;
        if (Math.toDegrees(this.InnerAngle) > 180) {
            this.InnerAngle = this.InnerAngle - Math.toRadians(180);
        }
        for (int x = 0; x < 8; x++) {
            V[x].X = Sign[x].X * (this.Width + (0.5 * this.Width * Math.cos(Math.toRadians(0))));
            V[x].Y = Sign[x].Y * (this.Height + (0.5 * this.Height * Math.cos(Math.toRadians(0))));
            V[x].Z = Sign[x].Z * (this.Depth + (0.5 * this.Depth * Math.cos(Math.toRadians(0))));
        }
        for (int x = 8; x < 16; x++) {
            V[x].X = Sign[x].X * (this.Width - (0.5 * this.Width * Math.cos(Math.toRadians(0))));
            V[x].Y = Sign[x].Y * (this.Height - (0.5 * this.Height * Math.cos(Math.toRadians(0))));
            V[x].Z = Sign[x].Z * (this.Depth - (0.5 * this.Depth * Math.cos(Math.toRadians(0))));
        }

        V[0].X = Sign[0].X * (this.Width + (0.5 * this.Width * Math.cos(this.InnerAngle)));
        V[0].Y = Sign[0].Y * (this.Height + (0.5 * this.Height * Math.cos(this.InnerAngle)));
        V[0].Z = Sign[0].Z * (this.Depth + (0.5 * this.Depth * Math.cos(this.InnerAngle)));
        V[8].X = Sign[8].X * (0.5 * this.Width);
        V[8].Y = Sign[8].Y * (0.5 * this.Height);
        V[8].Z = Sign[8].Z * (0.5 * this.Depth - (this.Depth * Math.sin(this.InnerAngle / 2)));
        V[9].X = Sign[9].X * (this.Width - (0.5 * this.Width * Math.cos(this.InnerAngle)));
        V[9].Y = Sign[9].Y * (this.Height - (0.5 * this.Height * Math.cos(this.InnerAngle)));
        V[9].Z = Sign[9].Z * (this.Depth - (0.5 * this.Depth * Math.cos(this.InnerAngle)));
        V[1].X = Sign[1].X * (1.5 * this.Width);
        V[1].Y = Sign[1].Y * (1.5 * this.Height);
        V[1].Z = Sign[1].Z * (1.5 * this.Depth - (3 * this.Depth * Math.sin(this.InnerAngle / 2)));

        V[3].X = Sign[3].X * (this.Width + (0.5 * this.Width * Math.cos(this.InnerAngle)));
        V[3].Y = Sign[3].Y * (this.Height + (0.5 * this.Height * Math.cos(this.InnerAngle)));
        V[3].Z = Sign[3].Z * (this.Depth + (0.5 * this.Depth * Math.cos(this.InnerAngle)));
        V[11].X = Sign[11].X * (0.5 * this.Width);
        V[11].Y = Sign[11].Y * (0.5 * this.Height);
        V[11].Z = Sign[11].Z * (0.5 * this.Depth - (this.Depth * Math.sin(this.InnerAngle / 2)));
        V[10].X = Sign[10].X * (this.Width - (0.5 * this.Width * Math.cos(this.InnerAngle)));
        V[10].Y = Sign[10].Y * (this.Height - (0.5 * this.Height * Math.cos(this.InnerAngle)));
        V[10].Z = Sign[10].Z * (this.Depth - (0.5 * this.Depth * Math.cos(this.InnerAngle)));
        V[2].X = Sign[2].X * (1.5 * this.Width);
        V[2].Y = Sign[2].Y * (1.5 * this.Height);
        V[2].Z = Sign[2].Z * (1.5 * this.Depth - (3 * this.Depth * Math.sin(this.InnerAngle / 2)));

        V[4].X = Sign[4].X * (this.Width + (0.5 * this.Width * Math.cos(this.InnerAngle)));
        V[4].Y = Sign[4].Y * (this.Height + (0.5 * this.Height * Math.cos(this.InnerAngle)));
        V[4].Z = Sign[4].Z * (this.Depth + (0.5 * this.Depth * Math.cos(this.InnerAngle)));
        V[12].X = Sign[12].X * (0.5 * this.Width);
        V[12].Y = Sign[12].Y * (0.5 * this.Height);
        V[12].Z = Sign[12].Z * (0.5 * this.Depth - (this.Depth * Math.sin(this.InnerAngle / 2)));
        V[13].X = Sign[13].X * (this.Width - (0.5 * this.Width * Math.cos(this.InnerAngle)));
        V[13].Y = Sign[13].Y * (this.Height - (0.5 * this.Height * Math.cos(this.InnerAngle)));
        V[13].Z = Sign[13].Z * (this.Depth - (0.5 * this.Depth * Math.cos(this.InnerAngle)));
        V[5].X = Sign[5].X * (1.5 * this.Width);
        V[5].Y = Sign[5].Y * (1.5 * this.Height);
        V[5].Z = Sign[5].Z * (1.5 * this.Depth - (3 * this.Depth * Math.sin(this.InnerAngle / 2)));

        V[7].X = Sign[7].X * (this.Width + (0.5 * this.Width * Math.cos(this.InnerAngle)));
        V[7].Y = Sign[7].Y * (this.Height + (0.5 * this.Height * Math.cos(this.InnerAngle)));
        V[7].Z = Sign[7].Z * (this.Depth + (0.5 * this.Depth * Math.cos(this.InnerAngle)));
        V[15].X = Sign[15].X * (0.5 * this.Width);
        V[15].Y = Sign[15].Y * (0.5 * this.Height);
        V[15].Z = Sign[15].Z * (0.5 * this.Depth - (this.Depth * Math.sin(this.InnerAngle / 2)));
        V[14].X = Sign[14].X * (this.Width - (0.5 * this.Width * Math.cos(this.InnerAngle)));
        V[14].Y = Sign[14].Y * (this.Height - (0.5 * this.Height * Math.cos(this.InnerAngle)));
        V[14].Z = Sign[14].Z * (this.Depth - (0.5 * this.Depth * Math.cos(this.InnerAngle)));
        V[6].X = Sign[6].X * (1.5 * this.Width);
        V[6].Y = Sign[6].Y * (1.5 * this.Height);
        V[6].Z = Sign[6].Z * (1.5 * this.Depth - (3 * this.Depth * Math.sin(this.InnerAngle / 2)));
    }

    public Group Render(Vertex[] V, Vertex Location, Vertex Angle, Color Col) {
        Animate();
        Group Rendered = new Group();
        int BrushSize = new Renderer().GetBrushSize(Location.X, Location.Y, Location.Z);
        if (new Renderer().InsidePlayerFOV(0, 0, 0, Location.X, Location.Y, Location.Z)) {
            // Render Outer Cube
            Rendered.getChildren().add(new Renderer().RenderLine(V[0], V[1], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[1], V[2], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[2], V[3], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[3], V[0], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[4], V[5], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[5], V[6], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[6], V[7], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[7], V[4], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[0], V[4], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[1], V[5], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[2], V[6], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[3], V[7], Location, Angle, Col, BrushSize));
            // Render Inner Cube
            Rendered.getChildren().add(new Renderer().RenderLine(V[0 + 8], V[1 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[1 + 8], V[2 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[2 + 8], V[3 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[3 + 8], V[0 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[4 + 8], V[5 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[5 + 8], V[6 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[6 + 8], V[7 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[7 + 8], V[4 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[0 + 8], V[4 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[1 + 8], V[5 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[2 + 8], V[6 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[3 + 8], V[7 + 8], Location, Angle, Col, BrushSize));
            // Render Corner Lines
            Rendered.getChildren().add(new Renderer().RenderLine(V[0], V[0 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[1], V[1 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[2], V[2 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[3], V[3 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[4], V[4 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[5], V[5 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[6], V[6 + 8], Location, Angle, Col, BrushSize));
            Rendered.getChildren().add(new Renderer().RenderLine(V[7], V[7 + 8], Location, Angle, Col, BrushSize));
        }
        return Rendered;
    }

}