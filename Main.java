package Source;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.util.Random;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application {

    static int ScreenWidth;
    static int ScreenHeight;
    int Scale = 8;
    Group root;
    Group World;
    Scene scene;
    Stage stage;
    PerspectiveCamera Camera = new PerspectiveCamera();
    Robot robot;
    int tick;
    Random Ran = new Random();
    static Entity Player;
    int NumberOfCubes = 1;
    int WorldSize = 5000;
    Entity[] Box = new Entity[NumberOfCubes];
    Entity[] EntityList;

    int SkyR = 0;
    int SkyG = 0;
    int SkyB = 0;

    static boolean HoldW;
    static boolean HoldS;
    static boolean HoldA;
    static boolean HoldD;
    static boolean HoldSPACE;
    static boolean HoldC;

    @Override
    public void start(Stage primaryStage) throws AWTException {
        robot = new Robot();
        root = new Group();
        World = new Group();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        ScreenWidth = (int) screenSize.getWidth();
        ScreenHeight = (int) screenSize.getHeight();

        scene = new Scene(root, 0, 0, true);
        scene.setFill(Color.BLACK);
        scene.setCursor(Cursor.NONE);
        scene.setCamera(Camera);
        stage = new Stage(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.setFullScreen(false);
        stage.setWidth(ScreenWidth);
        stage.setHeight(ScreenHeight);
        stage.setX(0);
        stage.setY(0);
        stage.show();
        handleKeyboard(scene, root);
        handleMouse(scene, root);
        handleScroll(scene, root);
        robot.mouseMove(ScreenWidth / 2, ScreenHeight / 2);
        Timer2();
    }

    private void handleKeyboard(Scene scene, final Node root) {
        // This Method Control Keyboard Input
        scene.setOnKeyPressed((KeyEvent event) -> {
            switch (event.getCode()) {
                case ESCAPE:
                    System.exit(0);
                    break;
                case W:
                    HoldW = true;
                    break;
                case S:
                    HoldS = true;
                    break;
                case A:
                    HoldA = true;
                    break;
                case D:
                    HoldD = true;
                    break;
                case SPACE:
                    HoldSPACE = true;
                    break;
                case C:
                    HoldC = true;
                    break;
            }
        });
        scene.setOnKeyReleased((KeyEvent event) -> {
            switch (event.getCode()) {
                case W:
                    HoldW = false;
                    break;
                case S:
                    HoldS = false;
                    break;
                case A:
                    HoldA = false;
                    break;
                case D:
                    HoldD = false;
                    break;
                case SPACE:
                    HoldSPACE = false;
                    break;
                case C:
                    HoldC = false;
                    break;
            }
        });
    }

    private void handleMouse(Scene scene, final Node root) {
        scene.setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
            }
            if (event.getButton().equals(MouseButton.SECONDARY)) {
            }
        });
    }

    private void handleScroll(Scene scene, final Node root) {
        scene.setOnScroll((ScrollEvent event) -> {
        });
    }

    public void Timer() {
        double Clock = System.nanoTime();
        double LastClock = Clock;
        boolean Running = true;
        double TicksPerSecond = 1.0;
        while (Running) {
            Clock = System.nanoTime();
            double DeltaClock = Clock - LastClock;
            if (DeltaClock >= 1000000000.0 / TicksPerSecond) {
                LastClock = Clock;
                Tick();
                Render();
            }
        }
    }

    public void Timer2() {
        AnimationTimer Animate = new AnimationTimer() {
            @Override
            public void handle(long now) {
                Tick();
                Render();
            }
        };
        Animate.start();
    }

    public void Tick() {
        //try {
        Physics();
        //} catch (Exception e) {
        //System.out.println("Fatal Error Occured - Twat");
        //}
        tick++;
    }

    public void Break() {
        //Used For Breaking The Program - Usually If You Want The Try Catch Statement To Run Exception
        int x = 1 / 0;
    }

    public void Physics() {
        if (tick % 10 == 0) {
            robot.mouseMove(ScreenWidth / 2, ScreenHeight / 2);
        }
        if (tick == 0) {
            Player = new Entity();
            Player.SetType("Player");
        }
        for (int x = 0; x < NumberOfCubes; x++) {
            if (tick == 0) {
                Box[x] = new Entity();
                Box[x].SetType("Cube");
                if (Ran.nextInt(10) == 0) {
                    Box[x].SetType("Tesseract");
                }
                Box[x].SetWidth(100);
                Box[x].SetHeight(100);
                Box[x].SetDepth(100);
                //Box[x].SetLocation(Ran.nextInt(WorldSize) - WorldSize / 2, Ran.nextInt(WorldSize) - WorldSize / 2, Ran.nextInt(WorldSize) - WorldSize / 2);
                //Box[x].SetVelocity((Ran.nextDouble() - 0.5) / 10, (Ran.nextDouble() - 0.5) / 10, (Ran.nextDouble() - 0.5) / 10);
                //Box[x].SetAngleVelocity((Ran.nextDouble() - 0.5) / 10, (Ran.nextDouble() - 0.5) / 10, (Ran.nextDouble() - 0.5) / 10);
                Box[x].SetColor(Ran.nextInt(255), Ran.nextInt(255), Ran.nextInt(255));
                Box[x].Create();
            }
            if (Box[x].GetLocation().X > WorldSize / 2 || Box[x].GetLocation().X < -WorldSize / 2) {
                Box[x].SetVelocity(-Box[x].GetVelocity().X, Box[x].GetVelocity().Y, Box[x].GetVelocity().Z);
            }
            if (Box[x].GetLocation().Y > WorldSize / 2 || Box[x].GetLocation().Y < -WorldSize / 2) {
                Box[x].SetVelocity(Box[x].GetVelocity().X, -Box[x].GetVelocity().Y, Box[x].GetVelocity().Z);
            }
            if (Box[x].GetLocation().Z > WorldSize / 2 || Box[x].GetLocation().Z < -WorldSize / 2) {
                Box[x].SetVelocity(Box[x].GetVelocity().X, Box[x].GetVelocity().Y, -Box[x].GetVelocity().Z);
            }
            Box[x].SetVelocity(1.001 * Box[x].GetVelocity().X, 1.001 * Box[x].GetVelocity().Y, 1.001 * Box[x].GetVelocity().Z);
            Box[x].SetAngleVelocity(1.0001 * Box[x].GetAngleVelocity().X, 1.0001 * Box[x].GetAngleVelocity().Y, 1.0001 * Box[x].GetAngleVelocity().Z);
            Box[x].Update();
        }
        Player.Update();
    }

    public void SetSkyColor() {
        SkyR = SkyR + Ran.nextInt(33) - 16;
        SkyG = SkyG + Ran.nextInt(33) - 16;
        SkyB = SkyB + Ran.nextInt(33) - 16;
        if (SkyR < 0) {
            SkyR = 0;
        }
        if (SkyR > 255) {
            SkyR = 255;
        }
        if (SkyG < 0) {
            SkyG = 0;
        }
        if (SkyG > 255) {
            SkyG = 255;
        }
        if (SkyB < 0) {
            SkyB = 0;
        }
        if (SkyB > 255) {
            SkyB = 255;
        }
        SkyR = 135;
        SkyG = 205;
        SkyB = 235;
        Color SkyColor = Color.rgb(SkyR, SkyG, SkyB);
        scene.setFill(SkyColor);
    }

    public void AddToEntityList(Entity[] EntityArray) {
        Entity[] TempEntityArray = EntityList;
        EntityList = new Entity[TempEntityArray.length + EntityArray.length];
        for (int x = 0; x < TempEntityArray.length; x++) {
            EntityList[x] = TempEntityArray[x];
        }
        for (int x = TempEntityArray.length; x < EntityList.length; x++) {
            EntityList[x] = EntityArray[x];
        }
    }

    public void SortEntityList() {
        Entity[] TempEntityArray = new Entity[EntityList.length];
        boolean[] EntityUsed = new boolean[EntityList.length];
        int CurrentEntityID;
        double SmallestLength;
        double CurrentLength;
        for (int x = 0; x < EntityList.length; x++) {
            CurrentEntityID = -1;
            SmallestLength = Integer.MAX_VALUE;
            for (int y = 0; y < EntityList.length; y++) {
                if (EntityUsed[y] == false) {
                    if (CurrentEntityID == -1) {
                        CurrentEntityID = y;
                        CurrentLength = new Renderer().GetRenderLength(EntityList[y].GetLocation().X, EntityList[y].GetLocation().Y, EntityList[y].GetLocation().Z);
                        SmallestLength = CurrentLength;
                        CurrentEntityID = y;
                    } else {
                        CurrentLength = new Renderer().GetRenderLength(EntityList[y].GetLocation().X, EntityList[y].GetLocation().Y, EntityList[y].GetLocation().Z);
                        if (CurrentLength < SmallestLength) {
                            SmallestLength = CurrentLength;
                            CurrentEntityID = y;
                        }
                    }
                }
            }
            TempEntityArray[x] = EntityList[CurrentEntityID];
            EntityUsed[CurrentEntityID] = true;
        }
        EntityList = TempEntityArray;
    }

    public void RenderEntityList() {
        for (int x = 0; x < EntityList.length; x++) {
            if (EntityList[x].Transparent == true) {
                World.getChildren().add(EntityList[x].Render());
            }
        }
        for (int x = EntityList.length - 1; x >= 0; x--) {
            if (EntityList[x].Transparent == false) {
                World.getChildren().add(EntityList[x].Render());
            }
        }
    }

    public void Render() {
        World = new Group();
        EntityList = new Entity[0];
        AddToEntityList(Box);
        SortEntityList();
        RenderEntityList();
        SetSkyColor();
        root = new Group(World);
        scene.setRoot(root);
    }

    public static void main(String[] args) {
        launch(args);
    }

}